#!/usr/bin/env python3

# build-mimeapps.py
# Copyright (C) 2019, Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from argparse import ArgumentParser, FileType

aparse = ArgumentParser(
    description="""
Transform default associations in custom format into standard XDG format.
"""
)
aparse.add_argument(
    "input", metavar="INPUT", type=FileType(), help="Custom format file to parse"
)
aparse.add_argument(
    "output", metavar="OUTPUT", type=FileType("w"), help="XDG format file to produce"
)
args = aparse.parse_args()

types = {}
app = None

for line in args.input:
    line = line.strip()
    if not line or line.startswith("#"):
        pass
    elif line.startswith("[") and line.endswith("]"):
        app = line[1:-1]
    elif app:
        apps = types.setdefault(line.lower(), [])
        if app not in apps:
            apps.append(app)
    else:
        raise RuntimeError("Missing section")

print("[Default Applications]", file=args.output)

for mime_type, apps in sorted(types.items()):
    print(mime_type, end="=", file=args.output)
    print(*apps, sep=";", end=";\n", file=args.output)

# Formatted using black
# vim:set et sw=4 tw=88:
